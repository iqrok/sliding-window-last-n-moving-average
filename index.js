const movingAverage = require('.');

const values = [
	10.5, 10, 10, 10, 10, 10, 10, 10, 10, 10,
	20, 20, 20, 20, 20, 20, 20, 20, 20, 20,
	30, 30, 30, 30, 50, 30, 30, 30, 30, 30,
	40, 40, 40, 40, 40, 40, 40, 40, 40, 40,
	50, 50, 50, 50, 50, 50, 50, 50, 50, 50,
	10, 10, 10, 10, 10, 10, 10, 10, 10, 10,
	20, 20, 20, 20, 20, 20.35, 20, 20, 20, 20,
	30, 30, 30, 30, 30, 30, 30, 30, 30, 30,
	40, 40, 40, 40, 40, 40, 40, 40, 40, 40,
	50, 50, 50, 50, 50, 50, 50, 50, 50, 50,
];

const N = 10;
const average = new movingAverage(N);

let index = 0;
const loop = setInterval(() => {

	const calc = average.add(values[index]);

	console.log(`${index} - moving average of last ${N} numbers:`, average.val);

	index++;

	if(index >= values.length){
		clearInterval(loop);
	}

});
