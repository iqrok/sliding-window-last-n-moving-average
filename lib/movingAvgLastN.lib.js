/**
 * This is just a nodejs port from codes found at :
 * http://www.zrzahid.com/moving-average-of-last-n-numbers-in-a-stream/
 * */

class MovingAvgLastN{
    /**
     * Create a moving average instance
     * @param {number} N - length of moving average numbers
     */
	constructor(N){
		this.maxTotal = N;
		this.lastN = [];
		this.avg = 0;
		this.head = 0;
		this.total = 0;
	}

	/**
	 * Add new number and calculate the average
	 * @param {number} num - number to add
	 * @throws Will throw error if num is not a number
	 * @returns {this} the method can be chained
	 * */
	add(num){
		if(isNaN(num)){
			throw `'${num}' is not a number!`;
		}

		let prevSum = this.total * this.avg;

		if(this.total == this.maxTotal){
			prevSum -= this.lastN[this.head];
			this.total--;
		}

		this.head = (this.head + 1) % this.maxTotal;
		const emptyPos = (this.maxTotal + this.head-1) % this.maxTotal;
		this.lastN[emptyPos] = num;

		const newSum = prevSum + num;
		this.total++;
		this.avg = newSum / this.total;

		return this;
	}

	/**
	 * Getter for current avergae value
	 * @returns {number}
	 * */
	get val(){
		return this.avg;
	}
}

module.exports = MovingAvgLastN;
